const path = require('path')
const WebpackUserscript = require('webpack-userscript')

module.exports = {
    mode: 'production',
    plugins: [
        new WebpackUserscript({
            headers: {
                name: 'Multiversal Paperclips',
                namespace: "https://gitlab.com/kassie3point11/multiversal-paperclips",
                version: '[version]-build.[buildNo]',
                match: 'https://decisionproblem.com/paperclips/index2.html'
            },
            proxyScript: {
                baseUrl: 'http://localhost:8080',
                enable: true
            }
        })
    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'multiversal-paperclips.user.js'
    },
    devServer: {
        hot: false,
        liveReload: false,
        webSocketServer: false
    }
}