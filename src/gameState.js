export default {
    stats: {
        get clips() {
            return window.clips
        },
        get funds() {
            return window.funds
        },
        get unsoldClips() {
            return window.unsoldClips
        },
        get pricePerClip() {
            return window.margin
        },
        get clipsPerSecond() {
            return window.clipmakerRate
        },
        get wireRemaining() {
            return window.wire
        },
        
    }
}